require_relative '../../constants'

class CreateTeamsTable < ActiveRecord::Migration[5.0]
  class Team < ActiveRecord::Base; end
  def self.up
    create_table :teams, force: true do |t|
      t.string :team_id, null: false
      t.string :name, null: false
      t.boolean :active, null: false, default: true
      t.string :domain
      t.string :token
      t.boolean :gif_enabled, null: false, default: false

      t.integer :join_bonus, null: false, default: 0 # per-user

      t.integer :max_user_join_bonus_count, null: false, default: 0
      t.integer :actual_user_join_bonus_count, null: false, default: 0

      t.integer :max_user_join_bonus_amount, null: false, default: 0
      t.integer :actual_user_join_bonus_amount, null: false, default: 0

      t.integer :max_admin_reward, null: false, default: 0
      t.integer :actual_admin_reward, null: false, default: 0 # how much has already been given to admin

      t.integer :max_introducer_reward, null: false, default: 0
      t.integer :actual_introducer_reward, null: false, default: 0

      t.timestamps
    end

    unless ENV['INDIUM_TOKEN'].blank?
      Team.create(
        team_id: SLACK_TEAMS[:indium][:id],
        name: SLACK_TEAMS[:indium][:name],
        active: true,
        token: ENV['INDIUM_TOKEN'],
        gif_enabled: false,
        join_bonus: (2*SLACK_USER_JOIN_REWARD),
        max_user_join_bonus_count: 100, # for now, give bonus to only first 100 users in this slack team
        max_user_join_bonus_amount: (100 * 2 * SLACK_USER_JOIN_REWARD)
      )
    end
  end

  def self.down
    drop_table :teams
  end
end
