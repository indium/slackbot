class CreateOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :offers do |t|
      t.references :user, null: false
      t.string :title, null: false
      t.text :desc
      t.string :category, null: false
      t.integer :price, limit: 8, null: false
      t.boolean :active, null: false
      t.timestamps
    end
  end
end
