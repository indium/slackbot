class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :platform, null: false
      t.string :team, null: false # will be same as platform except for Slack
      t.string :member # can be null if one user's account is created by another who only knows nickname, not userid
      t.string :nickname, null: false
      t.string :pubkey, null: false, unique: true
      t.string :encrypted_privkey, null: false
      t.string :encrypted_privkey_iv, null: false
      t.integer :balance, limit: 8, null: false, default: 0
      t.timestamp :balance_at
      t.timestamp :bonus_txn_at
      t.string :bonus_txn_id
      t.string :image_url
      t.json :omniauth
      t.timestamps
    end

    add_index :users, [:platform, :nickname], :unique => true
  end
end
