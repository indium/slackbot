class Balance < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "BALANCE: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"

    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    user = User.find_or_create_by(platform: platform, team: team_id, member: user_id)

    if user.persisted?
      balance = user.indium_balance
      msg = "<@#{data.user}> Your wallet (IPDB address: `#{user.pubkey}`) balance is: #{balance}."
      msg += "\nTry `pay <receiver> amount` to make a transfer." if balance > 0
      client.say(channel: data.channel, text: msg.with_env)
    else
      logger.info "BALANCE: platform = #{platform}, team_id = #{team_id}, user_id = #{user_id}, message = #{data.text}"
      client.say(channel: data.channel, text: "Something went wrong.".with_env)
    end

  end
end
