class Pay < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "PAY: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"

    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    sender = User.find_or_create_by(platform: platform, team: team_id, member: user_id)
    #binding.irb

    if !sender.persisted?
      logger.info "PAY: platform = #{platform}, team_id = #{team_id}, user_id = #{user_id}, message = #{data.text}"
      client.say(channel: data.channel, text: "Sender account INVALID: #{sender.errors.inspect}".with_env)
      return
    elsif /\A<@([^\s]+)> (\d+)\z/.match(_match['expression'])
      receiver, amount = /\A<@([^\s]+)> (\d+)\z/.match(_match['expression']).captures
      logger.info "p1: receiver: #{receiver} amount: #{amount}"
      receiver_account = User.find_or_create_by(platform: platform, team: team_id, member: receiver)
      #client.say(channel: data.channel, text: "You want to pay #{receiver} #{amount}?")
    elsif /\A(\d+) (?:TO)? <@([^\s]+)>\z/.match(_match['expression'])
      amount, receiver = /\A(\d+) <@([^\s]+)>\z/.match(_match['expression']).captures
      logger.info "p2: receiver: #{receiver} amount: #{amount}"
      receiver_account = User.find_or_create_by(platform: platform, team: team_id, member: receiver)
      #client.say(channel: data.channel, text: "You want to pay #{receiver} #{amount}?")
    elsif /\A([1-9A-Za-z]{43,45}) (\d+)\z/.match(_match['expression'])
      receiver, amount = /\A([1-9A-Za-z]{43,45}) (\d+)\z/.match(_match['expression']).captures
      logger.info "p3: receiver: #{receiver} amount: #{amount}"
      receiver_account = User.new(pubkey: receiver)
    elsif /\A(\d+) (?:TO)? ([1-9A-Za-z]{43,45})\z/.match(_match['expression'])
      amount, receiver = /\A(\d+) (?:TO)? ([1-9A-Za-z]{43,45})\z/.match(_match['expression']).captures
      logger.info "p4: receiver: #{receiver} amount: #{amount}"
      receiver_account = User.new(pubkey: receiver)
    else
      client.say(channel: data.channel, text: "Sorry, I couldn't understand that. Use `pay <receiver> <amount>`. Try `help` for other commands.".with_env)
      return
    end

    # sender.transfer_asset(receiver_account.pubkey, amount)
    receiver_pubkeys_amounts = [{:pubkey => receiver_account.pubkey, :amount => amount.to_i}]
    if sender.privkey.nil? || sender.privkey.empty?
      client.say(channel: data.channel, text: "We don't have your private key. Cannot proceed!".with_env)
      return
    end
    begin
      txn, message = INDIUM.transfer(receiver_pubkeys_amounts, sender.pubkey, sender.privkey)
      if txn
        message = "You have transferred #{amount.to_i} to #{receiver_account.slack_display_name(team_id)} successfully. Your new balance is #{sender.indium_balance}."
        client.say(channel: data.channel, text: message.with_env)
      else
        client.say(channel: data.channel, text: message.with_env)
      end
    rescue Exception => ex
      client.say(channel: data.channel, text: "Exception: #{ex.message} #{ex.backtrace}")
    end
  end
end
