class Bonus < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "BONUS: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"
    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user

    if _match['expression'].present?
      client.say(channel: data.channel, text: "Just say `bonus`!".with_env)
      return
    end

    user = User.find_or_create_by(platform: platform, team: team_id, member: user_id)

    if user.persisted?
      client.say(channel: data.channel, text: "Working...")
      begin
        status, message = Team.reward_user_for_joining(user, client.team)
        client.say(channel: data.channel, text: message.with_env)
      rescue Exception => ex
        client.say(channel: data.channel, text: "Exception: #{ex.message} #{ex.backtrace}")
      end
      # notice = "You can get 2000 more coins if you join the Indium slack ( https://indiumtalk.slack.com )!"
      # client.say(channel: data.channel, text: notice.with_env)
    else
      logger.info "BONUS: platform = #{platform}, team_id = #{team_id}, user_id = #{user_id}, message = #{data.text}"
      client.say(channel: data.channel, text: "Something went wrong: #{user.errors.inspect}".with_env)
    end

  end
end
