class Eval < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "EVAL: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"
    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    # client.say(channel: data.channel, text: "This command is disabled for now.")
    # return    
    user = User.find_or_create_by(platform: platform, team: team_id, member: user_id)

    # if user.invalid? || (user.team != SLACK_TEAMS[:indium][:id]) || (user.member != SLACK_USERS[:slackbotadmin])
    #  client.say(channel: data.channel, text: "Only admin can do remote-debugging.")
    #  return
    # end

    # if _match['expression'].start_with?((ENV['EVAL_SECRET'] || "secret") + " ")
      result = eval(_match['expression'])
      client.say(channel: data.channel, text: result.to_s.with_env)
    # else
    #   client.say(channel: data.channel, text: "Eval not allowed. Use secret?".with_env)
    # end
    #client.say(channel: data.channel, gif: 'help')
  end
end