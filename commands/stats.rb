class Stats < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "STATS: #{client.owner}, user=#{data.user}, message=#{data.text}, match=#{_match.inspect}"

    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user

    client.say(channel: data.channel, text: "Checking blockchain on IPDB...")
    msg = "Asset ID on IPDB: `#{INDIUM.asset_id}`"
    msg += "\nActive Teams: #{Team.active.count}"
    msg += "\n\nProminent Account balances:"
    msg += "\n*genesis* (`#{User.genesis.pubkey}`): #{User.genesis.indium_balance}"
    msg += "\n*dev* (`#{User.dev.pubkey}`): #{User.dev.indium_balance}"
    msg += "\n*advocacy* (`#{User.advocacy.pubkey}`): #{User.advocacy.indium_balance}"
    msg += "\n\nList of active teams using the bot: #{Team.active.all.collect(&:name).join(', ')}"
    client.say(channel: data.channel, text: msg.with_env)

  end
end