class Whoami < SlackRubyBot::Commands::Base
  def self.call(client, data, _match)
    logger.info "WHOAMI: #{client.owner}, user=#{data.user}, message=#{data.text} match=#{_match.inspect}"
    platform = "slack"
    team_id = client.team.id
    team_name = client.team.name
    user_id = data.user
    user = User.find_or_create_by(platform: platform, team: team_id, member: user_id)

    if user.valid?
      msg = "Hi <@#{user_id}>, your IPDB Address is: `#{user.pubkey}`. Account appears VALID"
    else
      msg = "Hi <@#{user_id}>, your IPDB Address is: `#{user.pubkey}`. Account appears INVALID: #{user.errors.inspect}"
    end
    client.say(channel: data.channel, text: msg.with_env)
  end
end
