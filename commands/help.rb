class Help < SlackRubyBot::Commands::Base
  HELP = <<-EOS.freeze
```
The following commands are supported:

pay        - make a transfer (pay @receiver <amount>)
balance    - check your account balance
whoami     - print your account information
stats      - print information about the Indium chain
help       - see this helpful message

```
NEWS: 

To invite other developers to this slack, use this link: https://join.slack.com/indiumtalk/shared_invite/MTk4MDQwNzMwMDIzLTE0OTczODIwNDItMzBkY2RlMjk0Yg

To join "Sawaal", our tip-based Q&A slack, go here: https://join.slack.com/sawaal/shared_invite/MjA2MDQzNzY3NjA3LTE0OTg2ODM5MjMtNTMwYjg5Zjg2Mw

Slackbot source code is available here: https://gitlab.com/indium/slackbot

Homepage: http://indium.org.in/
EOS
# echo               - useful for debugging while implementing commands
# eval               - remote debugging. Requires the secret keyphrase
# togglegif          - enable/disable GIFs: Only admin can run
  def self.call(client, data, _match)
    client.say(channel: data.channel, text: HELP.with_env)
    #client.say(channel: data.channel, gif: 'help')
    logger.info "HELP: #{client.owner}, user=#{data.user}"
  end
end
