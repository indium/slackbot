if ENV['RACK_ENV'].to_s == "production"
  SLACK_TEAMS = {
    indium: {name: 'Indium', id: 'T5P64TVP1'}
  }

  SLACK_USERS = {
    genesis: 'U5U0GSH2A', # will have a cap of releasing at most 5% coins each month and only to dev and advocacy accounts
    dev: 'U5U0G1WUE', # development fund, managed by the community
    advocacy: 'U5T8YCS5B', # advocacy fund, managed by the community
    slackuserpromo: 'U5XQ43HMZ', # pool account for rewarding new slackbot users
    slackadminpromo: 'U5XTRMTU4', # pool account for rewarding admins who install our bot
    nilesh: 'U5NBAPKEE', # founding developer, account will be public in the interest of full transparency
    slackbotadmin: 'U5YV53YRJ' # pool for funding the cost of servers and administration
  }
else
  SLACK_TEAMS = {
    indium: {name: 'IndiumTesting', id: 'T60S90BFY'}
  }

  SLACK_USERS = {
    genesis: 'U600HRX5Z',
    dev: 'U60T6B92T',
    advocacy: 'U604343MJ',
    slackuserpromo: 'U60SMMNSJ',
    slackadminpromo: 'U603ZSHT6',
    nilesh: 'U5ZC3PVNC',
    slackbotadmin: 'U604264KE'
  }
end
# these will increase in future. Don't be in a rush to spread the word too soon. the tech needs to become mature first.
# Discuss with the community first to maximize your reward.
# In the initial phase, this reward may not be automated, but based on the discretion of the advocacy committee
# So, again, check with the Indium community before marketing it prematurely.
# changes in the reward amounts will be made collaboratively and transparently
NEW_SLACK_INTRODUCER_REWARD = 10000
SLACK_OWNER_INSTALL_REWARD = 1000

# this will reduce in future
# TODO: total joining bonus should be capped at team-level
SLACK_USER_JOIN_REWARD = 1000

# If the above rewards seem small, do realize that bulk of the coins will be given
# for actual hard work towards development & advocacy. Join the community!! Not even 0.5% of the coins
# have been issued till now!

SLACK_TEAM_REGEXP = /\A[A-Z0-9]+\z/
SLACK_USER_REGEXP = /\A[A-Z0-9]+\z/
IPDB_ADDRESS_REGEXP = /\A[1-9A-Za-z][^OIl]{45,45}\z/

MAX_COINS = 100_000_000
MAX_COIN_MONTHLY_RELEASE_RATE = 0.05
DEV_FUND_RATIO = 0.4
ADVOCACY_FUND_RATIO = 1.0 - DEV_FUND_RATIO

if ENV['RACK_ENV'].to_s == "production"
  INDIUM = Indium.prod(ENV["IPDB_APP_ID"], ENV["IPDB_APP_KEY"])
else
  INDIUM = Indium.test(ENV["IPDB_APP_ID"], ENV["IPDB_APP_KEY"])
end

