class User < ActiveRecord::Base
  attr_encrypted :privkey, key: ENV['ENCRYPTION_KEY'], salt: ENV['ENCRYPTION_SALT']
  validates :platform, presence: true, inclusion: { in: %w(slack) }
  validates :member, presence: true, format: { with: /\A[a-zA-Z0-9]+\z/ }
  validates :team, presence: true, format: { with: /\A[a-zA-Z0-9]+\z/ }
  validate :slack_name_formats
  before_create :generate_key_pair

  def slack_name_formats
    errors.add(:team, "seems invalid for slack") unless team =~ SLACK_TEAM_REGEXP
    errors.add(:member, "seems invalid for slack") unless member =~ SLACK_USER_REGEXP
  end

  def generate_key_pair
    keypair = Bdb.generate_keys
    self.pubkey = keypair["public"]
    self.privkey = keypair["private"]
  end

  def indium_balance
    INDIUM.balance(self.pubkey)
  end

  def indium_pay(receiver_pubkey, amount)
    receiver_pubkeys_amounts = [{:pubkey => receiver_pubkey, :amount => amount}]
    INDIUM.transfer(receiver_pubkeys_amounts, self.pubkey, self.privkey)
  end

  def slack_display_name(team_id)
    if self.member.nil? && (self.pubkey =~ IPDB_ADDRESS_REGEXP)
      # this is IPDB address
      self.pubkey
    elsif self.team == team_id
      "<@#{self.member}>"
    else
      "Someone in a different team"
    end
  end

  # prominent accounts
  def self.genesis
    User.find_by(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:genesis])
  end

  def self.dev
    User.find_by(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:dev])
  end

  def self.advocacy
    User.find_by(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:advocacy])
  end

  def self.slack_admin_promo
    User.find_by(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:slackadminpromo])
  end

  def self.slack_user_promo
    User.find_by(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:slackuserpromo])
  end

  def self.slackbot_admin
    User.find_by(platform: "slack", team: SLACK_TEAMS[:indium][:id], member: SLACK_USERS[:slackbotadmin])
  end
end