require 'bigchaindb'
ipdb = { "url" => ENV["IPDB_URL"], "app_id" => ENV["IPDB_APP_ID"], "app_key" => ENV["IPDB_APP_KEY"]}
genesis = Bdb.generate_keys

# 10^12 total coins - EVER. (Bitcoin has 2.1x10^15 total satoshis and even further subdivisions are possible)
# Remember: For us, fractions aren't allowed so appropriate comparison is with satoshis, not bitcoins
# IPDB's max limit is 9x10^18 which fits in 64-bit unsigned int (postgres BIGINT type)
total_supply = 1_000_000_000_000

obj = Bdb.create_asset(ipdb, genesis["public"], genesis["private"], asset_data = {"name": "Indium", "symbol": "IND"}, total_supply, metadata = {"ts": Time.now.to_s})
raise obj unless obj && obj.is_a?(Hash)
indium = obj["txn"]["id"] # asset ID
bal = Bdb.balance_asset(ipdb, genesis["public"], indium)
raise if bal != total_supply
puts "genesis balance = #{bal}"

# We want to keep enough funds for future supply to manage inflation.
# if total supply is too high, we can always burn the money, but if it's too low,
# then nothing can be done. Releasing 10% of the pending supply every year seems to give
# a reasonable asymptotic curve
first_year_release = (0.1 * total_supply).round # 10%
first_year_fund = Bdb.generate_keys

receiver_pubkey_amounts = [
  {:pubkey => first_year_fund["public"], :amount => first_year_release}
]

obj2 = Bdb.transfer_asset(ipdb, receiver_pubkey_amounts, genesis["public"], genesis["private"], [obj["create"]], indium)

bal = Bdb.balance_asset(ipdb, genesis["public"], indium)
puts "genesis balance = #{bal}"
bal = Bdb.balance_asset(ipdb, first_year_fund["public"], indium)
puts "first_year_fund balance = #{bal}"

data = {
  indium: indium,
  genesis: genesis,
  first_year_fund: first_year_fund
}

puts data.inspect