### What's this?

This is a ChangeTip-like micropayment bot that integrates well in Slack communities. Based on https://github.com/slack-ruby/slack-ruby-bot

### Run

Join the test slack first with this link: https://join.slack.com/indiumtesting/shared_invite/MjA1MzA4MjYzMDkyLTE0OTg2ODk2OTYtZGI0NzM0ZDBjMw

```
# Install PostgreSQL and create a database called indium_development. Specify the credentials in ./launchdev file
# Get correct values of SLACK_CLIENT_SECRET and INDIUM_TOKEN from us and put them in ./launchdev
bundle install
./launchdev
```
Now ping @indiumtestingbot in the testing Slack.

For console: `bin/rails`
For clearing all tables: `./dberase`
For running all migrations: `./dbup` or `./launchdev`

When you start the bot with `./launchdev`, as long as it has a valid token in `teams` table’s first row (IndiumTesting), you will be able to interact with the bot. SLACK_CLIENT_ID and SLACK_CLIENT_SECRET are only needed for “Add To Slack” flow (which you don’t need for testing only the bot). Secrets committed in the repo are revoked. We'll be sharing the secrets with all devs but those will be periodically revoked.

For @indiumtestingbot, the database will be on your local machine. It’s not a typical client-server app. Bot and the testing slack are both connected to Slack over websockets. So the bot app does not need to be open on public internet and does not need to listen for requests on a port. It *makes* HTTP request and just needs a valid token.
