DONE - SlackBot should always stay up
* Database copies should be published publicly regularly - Currently on /list
PARTIALLY DONE - Transaction validation should be done in the database because we will soon need/have multiple apps besides slackbot
DONE - Transfer from one slack to another?
DONE - Implement joining bonus
* Reward for tipping
WIP - Reward for slack admin
DONE - Txn should be able to have comments
TODO - Bonus should be restricted team-wise.
DONE - set up a isolated testing environment
